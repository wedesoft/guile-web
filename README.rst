=============
Guile website
=============

This is an implementation of the new `website design proposed`_ for Guile.


.. REFERENCES
.. _website design proposed: http://lists.gnu.org/archive/html/guile-devel/2015-09/msg00014.html
