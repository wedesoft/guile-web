(begin
  (use-modules (srfi srfi-19))
  `((title . "Two years of Guile 2.0")
    (author . "Andy Wingo")
    (date unquote (make-date 0 0 0 0 19 2 2013 3600))
    (content
      div
      (p "February the 16th marked the "
         (a (@ (href "http://lists.gnu.org/archive/html/guile-devel/2011-02/msg00173.html"))
            "second anniversary of Guile 2.0")
         ".  Guile 2 was a major upgrade to Guile's performance and expressiveness as a language, and has become a pleasant foundation for its users."
         (br))
      (p "To celebrate, we "
         (a (@ (href "http://lists.gnu.org/archive/html/guile-user/2013-01/msg00007.html"))
            "organized")
         " a little birthday hack-feast -- a communal potluck of programs that Guilers brought together to share with each other.  Like "
         (a (@ (href "http://lists.gnu.org/archive/html/guile-user/2012-02/msg00048.html"))
            "last year")
         ", many people took on the challenge to come up with a dish, in one month."
         (br))
      (h3 "The Potluck's Dishes")
      (ul (li "Ian Price made "
              (a (@ (href "https://github.com/ijp/scheme-memcached"))
                 "portable Scheme bindings")
              " to Memcached, and he anticipates a first release in the near future.\n"))
      (ul (li "Daniel Hartwig and Andy Wingo hacked out a binding to OpenGL, using the dynamic FFI to generate an interface from the OpenGL specification.  They promise to release a first version soon, but for now, interested hackers should check out "
              (a (@ (href "http://gitorious.org/guile-figl"))
                 "the guile-figl page at gitorious")
              ".  Andy built a little demo around Figl, putting a bunch of particles in orbit around a central gravitational well.  Check out "
              (a (@ (href "http://wingolog.org/archives/2013/02/16/opengl-particle-simulation-in-guile"))
                 "his blog entry")
              " for more.\n"))
      (ul (li "Stefan Tampe conjured up a neat distributed computation system based on the ZeroMQ messaging library.  The "
              (a (@ (href "https://gitorious.org/guile-message/guile-message/blobs/master/DOCUMENTATION"))
                 "DOCUMENTATION file")
              " over at the gitorious page has all the lovely details.\n"))
      (ul (li "John Darrington "
              (a (@ (href "http://lists.gnu.org/archive/html/guile-user/2013-02/msg00143.html"))
                 "announced")
              " version 0.0 of a optimizing calculator for statisticians based on GNU PSPP.  The aim was not to bind PSPP directly, but to draw on its algorithms, while providing a more friendly Scheme-based interface.  The source code is available in a [git://de.cellform.com/pspp-experimental Git repo] at his site.\n"))
      (ul (li "Nala Ginrut hacked together a small web framework inspired by Ruby's Sinatra.  It comes with a small blog example to get you up and running.  The code is available on "
              (a (@ (href "https://gitorious.org/glow/artanis"))
                 "Gitorious")
              ".\n"))
      (ul (li "Andreas W "
              (a (@ (href "http://lists.gnu.org/archive/html/guile-user/2013-02/msg00137.html"))
                 "released")
              " a program "
              (a (@ (href "https://github.com/gpadd/guile-haiku"))
                 "that can display haikus")
              " taken from a database such as "
              (a (@ (href "http://www.gnu.org/fun/jokes/error-haiku.html"))
                 "those from GNU's humor pages")
              ".\n"))
      (ul (li "Mike Gran released an "
              (a (@ (href "http://lists.gnu.org/archive/html/guile-user/2013-02/msg00130.html"))
                 "updated version")
              " of his "
              (a (@ (href "http://www.lonelycactus.com/guile-curl.html"))
                 "cURL bindings")
              ".\n"))
      (ul (li "Mark Witmer wrote "
              (a (@ (href "http://lists.gnu.org/archive/html/guile-user/2013-02/msg00138.html"))
                 "XCB bindings")
              " for Guile, implemented as a language front-end for Guile's compiler whose input is the XML files used XCB.\n"))
      (ul (li "Ludovic Courtès came up with a "
              (a (@ (href "http://lists.gnu.org/archive/html/guile-user/2013-02/msg00131.html"))
                 "Boot-to-Guile QEMU image")
              ".  The image boots a Linux-Libre kernel with an initrd containing a copy of Guile, and where the +/init+ file is a Scheme program.  The image's build process is fully automated with "
              (a (@ (href "http://www.gnu.org/software/guix/"))
                 "GNU Guix")
              ".  This is a preview of what the Guix-based GNU distribution will soon use.\n"))
      (ul (li "Noah Lavine did some development work on the next generation of Guile's compiler and virtual machine, updating the continuation-passing-style intermediate language to the latest version of the development virtual machine.  This work will make Guile faster and more amenable to native compilation.  Peruse the "
              (a (@ (href "http://git.savannah.gnu.org/gitweb/?p=guile.git;a=shortlog;h=refs/heads/wip-rtl-cps"))
                 "wip-rtp-cps branch")
              " for the nitties and the gritties.\n"))
      (ul (li "Aleix Conchillo filled in two important missing pieces from Guile's web tool suite, implementing a JSON parser and emitter and an XML-RPC server and client.  Check out the code at the "
              (a (@ (href "http://github.com/aconchillo/guile-json"))
                 "guile-json")
              " and "
              (a (@ (href "http://github.com/aconchillo/guile-xmlrpc"))
                 "guile-xmlrpc")
              " projects on GitHub.\n"))
      (h3 "Conclusion")
      (p "Well, there you have it.  We hope you enjoy digesting these programs as much as we enjoyed writing them, and we hope to see you at our table next year."
         (br))
      (p "Happy birthday, Guile 2!" (br)))))
