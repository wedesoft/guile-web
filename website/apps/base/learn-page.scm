;;; Guile website --- GNU's extension language website
;;; Copyright © 2015 Luis Felipe López Acevedo <felipe.lopez@openmailbox.org>
;;;
;;; This file is part of Guile website.
;;;
;;; Guile website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Guile website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (website apps base learn-page)
  #:use-module (website apps base components)
  #:use-module (website utils)
  #:export (learn-page))


(define (learn-page)
  "Return the learn page in SXML."
  `(html
    (@ (lang "en"))
    ,(html-head "Learn"
		#:css (static-url "base/css/learn.css"))
    (body
     ,(site-menu #:active-link "Learn")
     (main
      (section
       (@ (class "page-summary centered noise-bg"))
       (h1 "Learn")
       (p "Use the documentation listed here to get you started using Guile. "
	  "If you get stuck and need some help to figure something out, "
	  "don't be afraid to ask the Guile community that hangs out at the "
	  (a (@ (href "https://webchat.freenode.net/?channels=%23guile"))
	     "#guile")
	  " channel on the Freenode network, or subscribe to the "
	  (a (@ (href "https://lists.gnu.org/mailman/listinfo/guile-user/"))
	     "guile-users")
	  " mailing list. Also, don't forget to exercise your "
	  (a (@ (href "http://www.gnu.org/philosophy/free-sw.en.html"))
	     "freedom")
	  " to study the source code of the "
	  (a (@ (href ,(site-url "libraries/")))
	     "software written with Guile")
	  "; that's another great way to learn!"))
      (div
       (@ (class "relative"))
       (nav
	(@ (class "toc"))
	(h2 "Contents")
	(ul
	 (li (a (@ (href "#tutorials")) "Tutorials"))
	 (li (a (@ (href "#manuals")) "Reference manuals"))
	 (li (a (@ (href "#scheme-resources")) "Scheme resources"))
	 (li (a (@ (href "#bibliography")) "Suggested bibliography"))
	 (li (a (@ (href ,(site-url "docs/faq/guile-faq.html"))) "FAQ"))))
       (section
	(@ (class "sheet"))
	(h2 (@ (id "tutorials")) "Tutorials")
	;; TODO: Write the First Steps Guide to uncomment
	;; (section
	;;  (@ (class "resource with-cover"))
	;;  (img (@ (src ,(static-url "base/img/covers/guile-first-steps.png"))
	;; 	 (alt "")))
	;;  (h3
	;;   (a (@ (href ,(site-url "docs/first-steps/")))
	;;      "GNU Guile 2.0 First Steps Guide"))
	;;  (p "An introductory guide to programming with Guile that does not "
	;;     "exist yet but we would like to have..."))
	(section
	 (@ (class "resource"))
	 (h3
	  (a (@ (href ,(site-url "docs/guile-tut/tutorial.html")))
	     "Tortoise: Extending a C program with Guile"))
	 (p "This tutorial by Daniel Kraft explains step-by-step how to use "
	    "guile in a straightforward Logo-like application."))

	(h2 (@ (id "manuals")) "Reference manuals")

	(section
	 (@ (class "resource with-cover"))
	 (img (@ (src ,(static-url "base/img/covers/guile-reference.png"))
		 (alt "")))
	 (h3
	  (a (@ (href ,(site-url "manual/"))) "Guile 2.2"))
	 (p "The current stable release series. Guile's reference manual is
also included in each Guile distribution and should be accessible via Emacs'
Info mode once you have installed Guile.")
         )

	(p "Reference manuals for future and older versions of Guile:")
	(ul
	 (li (a (@ (href ,(site-url "docs/master/guile.html/")))
		"Guile 3.0") " (development branch)")
	 (li (a (@ (href ,(site-url "docs/docs-2.0/guile-ref/")))
		"Guile 2.0") " (the old stable release series)")
	 (li (a (@ (href ,(site-url "docs/docs-1.8/guile-ref/")))
		"Guile 1.8") " (the older stable release series)")
	 (li (a (@ (href ,(site-url "docs/docs-1.6/guile-ref/")))
		"Guile 1.6") " (the ancient stable release series)"))
        (p "Hard copies of the Guile 2.0 Reference Manual are "
           (a (@ (href "http://www.network-theory.co.uk/guile/manual/"))
              "available from Network Theory, Ltd") ".")
	(h2 (@ (id "scheme-resources")) "Scheme resources")
	(section
	 (@ (class "resource with-cover"))
	 (img (@ (src ,(static-url "base/img/covers/r6rs.jpg")) (alt "")))
	 (h3
	  (a (@ (href "http://www.r6rs.org/")) "The Revised" (sup "6")
	     " Report on the Algorithmic Language Scheme"))
	 (p "The most recent Scheme standard, for which Guile has partial "
	    "support. This report includes an overview of Scheme and a "
	    "formal definition of the language and its standard libraries."))
	(section
	 (@ (class "resource"))
	 (h3
	  (a (@ (href "http://www-swiss.ai.mit.edu/~jaffer/r5rs_toc.html"))
	     "The Revised" (sup "5")
	     " Report on the Algorithmic Language Scheme"))
	 (p "The previous Scheme standard. Guile implements it."))
	(section
	 (@ (class "resource"))
	 (h3
	  (a (@ (href "http://www.cs.indiana.edu/scheme-repository/R4RS/r4rs_toc.html"))
	     "The Revised" (sup "4")
	     " Report on the Algorithmic Language Scheme"))
	 (p "An older revision of the Scheme standard."))
	(section
	 (@ (class "resource"))
	 (h3
	  (a (@ (href "http://scheme-reports.org/"))
	     "Scheme Reports web site"))
	 (p "Where standardization discussions happen."))
	(section
	 (@ (class "resource"))
	 (h3
	  (a (@ (href "http://www.cs.indiana.edu/scheme-repository/home.html"))
	     "The Internet Scheme Repository"))
	 (p "A good source of links, code, and documentation for scheme."))
	(section
	 (@ (class "resource"))
	 (h3
	  (a (@ (href "http://www.schemers.org/")) "Schemers.org"))
	 (p "A list of Scheme resources including documentation, software, "
	    "communities, jobs, and events."))
	(section
	 (@ (class "resource"))
	 (h3
	  (a (@ (href "http://groups.csail.mit.edu/mac/projects/scheme/"))
	     "MIT's Scheme home page"))
	 (p "Not quite as extensive as the Scheme Repository or Schemers.org."))
	(section
	 (@ (class "resource"))
	 (h3
	  (a (@ (href "http://srfi.schemers.org/"))
	     "Scheme Requests for Implementation"))
	 (p "Contains proposals for a number of Scheme extensions. Many of these tend to exist in one form or the other in a lot of the different Schemes, but can have wildly different interfaces. The SRFI process is essentially aimed at making these non-standard features more standard, without actually being standard (see also: "
	    (a (@ (href "http://www.dict.org/bin/Dict?Form=Dict1&Query=obfuscated&Strategy=*&Database=*")) "obfuscated")
	    "). Guile 1.8 implements a large number of SRFIs."))
	(section
	 (@ (class "resource"))
	 (h3
	  (a (@ (href "http://www.cs.washington.edu/education/courses/341/99su/lectures/scheme/"))
	     "Greg Badros' Scheme Lecture Notes"))
	 (p "A set of slides that touch on a variety of subjects related to scheme programing, including samples of similar code written in C and scheme, lambda calculus, and lists."))
	(h2 (@ (id "bibliography")) "Suggested bibliography")
	(section
	 (@ (class "resource with-cover"))
	 (img (@ (src ,(static-url "base/img/covers/sicp.jpg")) (alt "")))
	 (h3
	  (a (@ (href "https://mitpress.mit.edu/sicp/"))
	     "Structure and Interpretation of Computer Programs (SICP)"))
	 (p "An influential text used at MIT's as the basis for entry-level "
	    "computer science courses. The material is aimed at instructors "
	    "using SICP as a course text, and at people using the book for "
	    "self-study.")))))
     ,(site-footer))))
