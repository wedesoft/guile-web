;;; Guile website --- GNU's extension language website
;;; Copyright © 2015 Luis Felipe López Acevedo <felipe.lopez@openmailbox.org>
;;; Copyright © 2015 Mathieu Lirzin <mthl@gnu.org>
;;;
;;; This file is part of Guile website.
;;;
;;; Guile website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Guile website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (website apps base libraries-page)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-26)
  #:use-module (texinfo)
  #:use-module (texinfo html)
  #:use-module (website apps base components)
  #:use-module (website utils)
  #:export (libraries-page))

(define (texi->shtml str)
  "Return a SHTML representation of texinfo fragment STR."
  ;; 'texi-fragment->stexi' uses a string port so make sure it's a
  ;; Unicode-capable one (see <http://bugs.gnu.org/11197>.)
  (with-fluids ((%default-port-encoding "UTF-8"))
    (stexi->shtml (texi-fragment->stexi str))))

;;; A package entry.
(define-record-type <package>
  (make-package name description url tags license)
  package?
  (name        package-name)            ;string
  (description package-description)     ;string
  (url         package-url)             ;string
  (tags        package-tags)            ;list of strings
  (license     package-license))        ;string

(define* (package #:key name description url tags license)
  "Convenient procedure which uses keywords when defining a package entry."
  (make-package name description url tags license))

(define (package->shtml pkg)
  "Return an SHTML representation of package PKG."
  `(section
    (@ (class "lib"))
    (h3 (a (@ (href ,(package-url pkg)))
           ,(package-name pkg)))
    (p ,@(map (lambda (tag)
                `(span (@ (class "guile-version-tag")) ,tag))
              (package-tags pkg)))
    ,(texi->shtml (package-description pkg))
    (p (b "License: ") ,(package-license pkg))))


;;;
;;; Core.
;;;

(define guile-lib
  (package #:name "Guile-lib"
           #:description "Guile-Lib is intended as an accumulation place for
pure-scheme Guile modules, allowing for people to cooperate integrating their
generic Guile modules into a coherent library.  Think \"a down-scaled,
limited-scope CPAN for Guile\"."
           #:url "http://www.nongnu.org/guile-lib/"
           #:tags '("Guile 2.2" "Guile 2.0" "Guile 1.8")
           #:license "LGPL 2.1 or later"))

(define guile-lint
  (package #:name "Guile-lint"
           #:description "Syntactic and semantic checks on Guile 1.8 programs
and modules. Picks up things like unbound variables in hard-to-reach
places (note that Guile 2.0 brings part of Guile-Lint's functionality through
its @uref{manual/html_node/Compilation.html, compiler warnings.})"
           #:url "http://user42.tuxfamily.org/guile-lint/index.html"
           #:tags '("Guile 1.8")
           #:license "GPL 2 or later"))

(define guile-reader
  (package #:name "Guile-Reader"
           #:description "Guile-Reader is a toolkit to build readers for
Guile, making it easy to extend the syntax. It makes it possible to have
several coexisting readers, recognizing different syntaxes."
           #:url "http://www.nongnu.org/guile-reader/"
           #:tags '("Guile 2.2" "Guile 2.0" "Guile 1.8")
           #:license "LGPL 2.1 or later"))

(define guile-syntax-parse
  (package #:name "Guile syntax parse"
           #:description "Guile syntax parse is a port of Racket's syntax
parse for Guile 2. It allows one to parse complex syntax expressions correctly
and with good error reporting in a hygienic way.  The recommending reading for
anyone interested to learn more is the Racket documentation for syntax parse
at @uref{http://docs.racket-lang.org/syntax/Parsing_Syntax.html, Racket doc}."
           #:url "https://gitlab.com/guile-syntax-parse/guile-syntax-parse"
           #:tags '("Guile 2.0")
           #:license "LGPL 2 or later"))

(define (%packages-core)
  "These projects modify or enhance the innards of Guile."
  (list guile-lib guile-lint guile-reader guile-syntax-parse))


;;;
;;; Graphical User Interfaces.
;;;

(define guile-clutter
  (package #:name "GNU Guile-Clutter"
           #:description "Guile wrapper for GNOME Clutter library. Clutter is a
Scene Graph based canvas working in Retained Mode. Every object on the scene
is usually a 2D surface inside a 3D space."
           #:url "http://www.gnu.org/software/guile-gnome/"
           #:tags '("Guile 2.0")
           #:license "GPL 2 or later"))

(define guile-gnome
  (package #:name "GNU Guile-Gnome"
           #:description "Guile wrapper collection for GNOME core libraries. It
provides a comprehensive environment for developing modern graphical
applications."
           #:url "https://www.gnu.org/software/guile-gnome/"
           #:tags '("Guile 2.0")
           #:license "GPL 2 or later"))

(define guile-gtk
  (package #:name "GNU Guile-GTK"
           #:description "Guile-GTK provides bindings to the GTK+ toolkit from
Guile 1.8."
           #:url "http://www.gnu.org/software/guile-gtk/"
           #:tags '("Guile 1.8")
           #:license "GPL 3 or later"))

(define guile-ncurses
  (package #:name "GNU Guile-Ncurses"
           #:description "GNU Guile-Ncurses is a Guile library that
provides functions for creating text user interfaces. The text user
interface functionality is built on the
@uref{https://www.gnu.org/software/ncurses/ncurses.html, ncurses}
libraries: curses, form, panel, and menu."
           #:url "http://www.gnu.org/software/guile-ncurses/"
           #:tags '("Guile 2.0")
           #:license "GPL 3 or later"))

(define guile-sdl
  (package #:name "GNU Guile-SDL"
           #:description "Guile-SDL is a set of modules that provide bindings
for @uref{http://www.libsdl.org/, SDL} (Simple DirectMedia Layer) to enable
Guile programmers to do all the nice things you can do with SDL."
           #:url "http://www.gnu.org/software/guile-sdl/"
           #:tags '("Guile 2.0")
           #:license "GPL 3 or later"))

(define libruin
  (package #:name "libRUIN"
           #:description "libRUIN (Renderer for User Interfaces in Ncurses) is
a rendering library for various XML-based user interface markup
languages (such as Mozilla XUL), using the Ncurses terminal control library as
a rendering target. GNU Guile and the SDOM Scheme module are used as the
\"glue\" that manages user input and event handling."
           #:url "http://www.nongnu.org/libruin/"
           #:tags '("Guile 2.0")
           #:license "GPL"))

(define guile-sdl2
  (package #:name "Guile-SDL2"
           #:description "Guile-SDL2 provides bindings for the SDL2 C
shared library.  The bindings are written in pure Scheme using Guile's
foreign function interface."
           #:url "https://dthompson.us/projects/guile-sdl2.html"
           #:tags '("Guile 2.2" "Guile 2.0")
           #:license "LGPL 3 or later"))

(define chickadee
  (package #:name "Chickadee"
           #:description "Chickadee is a game development toolkit for
Guile built on top of SDL2 and OpenGL."
           #:url "https://dthompson.us/projects/chickadee.html"
           #:tags '("Guile 2.2" "Guile 2.0")
           #:license "GPL 3 or later"))

(define (%packages-gui)
  "Projects that provide support for the creation of graphical user
interfaces (GUIs)."
  (list guile-clutter guile-gnome guile-gtk guile-ncurses guile-sdl libruin
        guile-sdl2 chickadee))


;;;
;;; File formats.
;;;

(define guile-dsv
  (package #:name "Guile-DSV"
           #:description "Delimiter-separated values (DSV) format
parser for GNU Guile.  Guile-DSV supports both the
@uref{http://www.catb.org/~esr/writings/taoup/html/ch05s02.html#id2901882,
Unix-style DSV} format and @uref{https://tools.ietf.org/html/rfc4180,
RFC 4180} format."
           #:url "https://github.com/artyom-poptsov/guile-dsv"
           #:tags '("Guile 2.0")
           #:license "GNU GPL v3 or later"))

(define guile-ics
  (package #:name "Guile-ICS"
           #:description "iCalendar (@uref{https://tools.ietf.org/html/rfc5545,
RFC5545}) format parser for GNU Guile."
           #:url "https://github.com/artyom-poptsov/guile-ics"
           #:tags '("Guile 2.0")
           #:license "GNU GPL v3 or later"))

(define (%packages-file-formats)
  "Tools and libraries to read and produce specific file formats."
  (list guile-dsv guile-ics))



;;;
;;; Databases.
;;;

(define guile-wiredtiger
  (package #:name "guile-wiredtiger"
           #:description "guile-wiredtiger is a database library
powered by wiredtiger.
It comes with two high level abstraction: the EAV model and a graph
database.
The EAV database use logic extension of Scheme microkanren to execute
queries.
The graph database use functional streams for querying."
           #:url "https://framagit.org/a-guile-mind/guile-wiredtiger/"
           #:tags '("Guile 2.2")
           #:license "GPL 2 or later"))

(define (%packages-database)
  "Database tools and libraries."
  (list guile-wiredtiger))


;;;
;;; Guile modules for networking.
;;;

(define artanis
  (package #:name "GNU Artanis"
           #:description "Web Application Framework (WAF) with support for
templates, MVC, relational mapping, and more."
           #:url "http://web-artanis.com/"
           #:tags '("Guile 2.0")
           #:license "GPLv3+ and LGPLv3+"))

(define haunt
  (package #:name "Haunt"
           #:description "Haunt is a static site generator.  Haunt
features a functional build system and an extensible interface for
reading articles in any format."
           #:url "https://haunt.dthompson.us"
           #:tags '("Guile 2.2" "Guile 2.0")
           #:license "GPL 3 or later"))

(define gnutls
  (package #:name "GnuTLS"
           #:description "Guile bindings for the GnuTLS library, an
implementation of the Transport Layer Security (TLS) protocol."
           #:url "http://gnutls.org/"
           #:tags '("Guile 2.2" "Guile 2.0" "Guile 1.8")
           #:license "LGPL 3 or later"))

(define guile-avahi
  (package #:name "Guile-Avahi"
           #:description "Bindings for Avahi, an implementation of the mDNS and
DNS-SD protocol. The latter provides service discovery mechanisms."
           #:url "http://www.nongnu.org/guile-avahi/"
           #:tags '("Guile 1.8")
           #:license "LGPL 3 or later"))

(define guile-rpc
  (package #:name "GNU Guile-RPC"
           #:description "GNU Guile-RPC is an implementation of ONC RPC and
XDR (standardized as @uref{http://tools.ietf.org/html/rfc1831.html, RFC 1831}
and @uref{http://tools.ietf.org/html/rfc4506.html, RFC 4506}) in Guile Scheme,
and for use by Guile programs."
           #:url "http://www.gnu.org/software/guile-rpc/"
           #:tags '("Guile 2.0")
           #:license "LGPL 3 or later"))

(define scss
  (package #:name "SCSS"
           #:description "SCSS is an implementation in Scheme of the W3C CSS
recommendation (version 2.1). It provides a framework for parsing
user-supplied style information and for querying the resulting cascade using
SXML/SDOM documents or plain text."
           #:url "http://www.nongnu.org/scss/"
           #:tags '("Guile 2.0")
           #:license "GPL"))

(define guile-ssh
  (package #:name "Guile-SSH"
           #:description "Guile-SSH is a library that provides access
to the @uref{https://en.wikipedia.org/wiki/Secure_Shell, SSH} protocol
for GNU Guile programs.  It is a wrapper to the underlying
@uref{https://www.libssh.org/, libssh} library."
           #:url "https://github.com/artyom-poptsov/guile-ssh"
           #:tags '("Guile 2.0")
           #:license "GNU GPL v3 or later"))

(define (%packages-networking)
  "These projects provide Guile modules for networking."
  (list artanis gnutls guile-avahi guile-rpc guile-ssh haunt scss))


;;;
;;; Tools implemented with Guile.
;;;

(define aiscm
  (package #:name "AIscm"
           #:description "Guile numerical arrays and tensor extension.
AIscm is a Guile extension providing multi-dimensional arrays and tensor
operations. Array elements can be scalars or composite values such as complex
numbers or RGB values. Array operations are provided as GOOPS generics. Real
time performance is achieved using an AMD64 JIT compiler. Furthermore AIscm
provides input/output bindings for ImageMagick, V4L2, FFmpeg, PulseAudio, and
X.Org."
           #:url "http://www.gnu.org/software/autogen"
           #:tags '("Guile 2.2" "Guile 2.0")
           #:license "GPL 3 or later"))

(define autogen
  (package #:name "GNU AutoGen"
           #:description "AutoGen is a tool designed for generating program
files that contain repetitive text with varied substitutions. Its goal is to
simplify the maintenance of programs that contain large amounts of repetitious
text. This is especially valuable if there are several blocks of such text
that must be kept synchronized."
           #:url "http://www.gnu.org/software/autogen"
           #:tags '("Guile 2.0")
           #:license "GPL for the AutoGen engine, LGPL for the POSIX and GNU
compliant option processing"))

(define guix
  (package #:name "GNU Guix"
           #:description "GNU Guix is a purely functional package manager, and
associated distribution of the GNU system. In addition to standard package
management features, Guix supports transactional upgrades and roll-backs,
unprivileged package management, per-user profiles, and garbage collection. It
provides Guile Scheme APIs, including high-level embedded domain-specific
languages (EDSLs), to describe how packages are to be built and composed."
           #:url "http://www.gnu.org/software/guix"
           #:tags '("Guile 2.2" "Guile 2.0")
           #:license "GPL 3 or later"))

(define libchop
  (package #:name "Libchop"
           #:description "Libchop is a set of utilities and library for data
backup and distributed storage. Its main application is chop-backup, an
encrypted backup program that supports data integrity checks, versioning at
little cost, distribution among several sites, selective sharing of stored
data, adaptive compression, and more. The library itself, which chop-backup
builds upon, implements storage techniques such as content-based addressing,
content hash keys, Merkle trees, similarity detection, and lossless
compression. It is written in C and Scheme, with Guile 2.0 bindings."
           #:url "http://nongnu.org/libchop/"
           #:tags '("Guile 2.0")
           #:license "GPL 3 or later"))

(define (%packages-tools)
  "These projects are tools for or implemented with Guile. Mostly, these are
the types of things that automate documentation or code generation."
  (list aiscm autogen guix libchop))


;;;
;;; Applications.
;;;

(define beast
  (package #:name "Beast"
           #:description "Beast is a music composition and synthesis tool."
           #:url "http://beast.gtk.org/"
           #:tags '("Guile 2.0")
           #:license "GPL 2 or later, LGPL 2.1 or later"))

(define gdb
  (package #:name "GNU GDB"
           #:description "GDB is the GNU debugger.  With it, you can monitor
what a program is doing while it runs or what it was doing just before a
crash.  It allows you to specify the runtime conditions, to define
breakpoints, and to change how the program is running to try to fix bugs.  It
can be used to debug programs written in C, C++, Ada, Objective-C, Pascal and
more."
           #:url "https://www.gnu.org/software/gdb/"
           #:tags '("Guile 2.0")
           #:license "GPL 3 or later"))

(define geda
  (package #:name "Lepton-EDA (GPL Electronic Design Automation)"
           #:description "The Lepton-EDA (formerly gEDA) project provids a
full GPL-licensed suite of electronic design tools, including schematic
capture, simulation, prototyping and production. Guile is used for
extensibility and customisation of many gEDA applications."
           #:url "https://github.com/lepton-eda/"
           #:tags '("Guile 2.0")
           #:license "GPL 2 or later"))

(define lilypond
  (package #:name "GNU Lilypond"
           #:description "LilyPond generates beautiful music notation from an
input file."
           #:url "http://lilypond.org/"
           #:tags '("Guile 1.8")
           #:license "GPL"))

(define skribilo
  (package #:name "Skribilo"
           #:description "Skribilo is a document authoring system that can
produce output in a variety of formats including HTML and PDF from a single
input format. It is implemented in Scheme and highly customizable and
programmable."
           #:url "http://www.nongnu.org/skribilo/"
           #:tags '("Guile 2.0" "Guile 1.8")
           #:license "GPL 2 or later"))

(define snd
  (package #:name "Snd"
           #:description "Snd is a sound editor."
           #:url "http://www-ccrma.stanford.edu/software/snd/"
           #:tags '("Guile 2.0")
           #:license "LGPL"))

(define texmacs
  (package #:name "GNU TeXmacs"
           #:description "GNU TeXmacs is a free WYSIWYW editing platform with
special features for scientists. The software aims to provide a unified and
user friendly framework for editing structured documents with different types
of content (text, graphics, mathematics, interactive content, etc.). The
rendering engine uses high-quality typesetting algorithms so as to produce
professionally looking documents, which can either be printed out or presented
from a laptop. New presentation styles can be written by the user and new
features can be added to the editor using the Scheme extension language."
           #:url "http://texmacs.org/"
           #:tags '("Guile 2.0")
           #:license "GPL"))

(define xchat-guile
  (package #:name "XChat-Guile"
           #:description "XChat-Guile is a plugin for XChat that enables XChat
plugin writers to write their plugins in Scheme language.  Since it uses
libguile, the plugin writers can do almost everything that they can do with
guile.  e.g It is possible to do all kinds of gnome stuff using the
guile-gnome modules."
           #:url "http://static.fi/~zeenix/xchat-guile/"
           #:tags '("Guile 2.0")
           #:license "GPL"))

(define (%packages-apps)
  "These projects are applications using Guile: either C programs that allow
extension via Scheme, or Scheme programs written for Guile."
  (list beast gdb geda lilypond skribilo snd texmacs xchat-guile))


;;;
;;; Categories.
;;;

;;; A package category.
(define-record-type <category>
  (make-category name abbrev id packages)
  category?
  (name     category-name)              ;string
  (abbrev   category-abbrev)            ;string
  (id       category-id)                ;string
  (packages category-packages))         ;thunk

(define* (category name packages #:key abbrev id)
  ;; Convenient procedure to instantiate a package category.
  (make-category name abbrev id packages))

(define (category->shtml ctgy)
  "Return an SHTML representation of a category and the packages which
belong to this category."
  (let ((thunk (category-packages ctgy)))
    `((h2 (@ (id ,(category-id ctgy))) ,(category-name ctgy))
      (p ,(procedure-documentation thunk))
      ,@(map-in-order package->shtml (thunk)))))

(define %categories
  ;; List of all package categories.
  (map-in-order
   (cut apply category <>)
   `(("Core" ,%packages-core #:id "core")
     ("Graphical User Interfaces" ,%packages-gui #:id "gui" #:abbrev "GUI")
     ("File Formats" ,%packages-file-formats #:id "file-formats")
     ("Databases" ,%packages-database #:id "databases")
     ("Networking" ,%packages-networking #:id "networking")
     ("Tools" ,%packages-tools #:id "tools")
     ("Applications" ,%packages-apps #:id "apps" ))))


;;;
;;; Libraries page.
;;;

(define (libraries-page)
  "Return the libraries page in SXML."
  `(html
    (@ (lang "en"))
    ,(html-head "Libraries"
                #:css (static-url "base/css/libraries.css"))
    (body
     ,(site-menu #:active-link "Libraries")
     (main
      (section
       (@ (class "page-summary centered noise-bg"))
       (h1 "Libraries")
       (p "This page lists free software projects that use or enhance the
current stable version of Guile. To have your project listed here or to
contribute, ask for instructions on the "
          (a (@ (href "https://lists.gnu.org/mailman/listinfo/guile-user/"))
             "guile-user") " mailing list."))
      (div
       (@ (class "relative"))
       (nav (@ (class "toc"))           ;table of contents
            (h2 "Categories")
            (ul
             ,@(map-in-order
                (lambda (ctgy)
                  ;; Generate an entry in the table of contents.
                  (let ((name (category-name ctgy))
                        (abbr (category-abbrev ctgy)))
                    `(li (a (@ (href ,(string-append "#" (category-id ctgy))))
                            ,(if abbr
                                 `(abbr (@ (title ,name)) ,abbr)
                                 name)))))
                %categories)))
       (section
        (@ (class "sheet"))
        ;; Generate the package categories.
        ,@(fold-right (lambda (ctgy acc)
                        (append (category->shtml ctgy) acc))
                      '()
                      %categories))))
     ,(site-footer))))
