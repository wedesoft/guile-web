;;; Guile website --- GNU's extension language website
;;; Copyright © 2015 Luis Felipe López Acevedo <felipe.lopez@openmailbox.org>
;;;
;;; This file is part of Guile website.
;;;
;;; Guile website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Guile website.  If not, see <http://www.gnu.org/licenses/>.

;;; This module defines a list of Web resources provided by this application,
;;; such as HTML pages, RSS feeds, and any other resources that can be expressed
;;; in SXML.

(define-module (website apps base base-resources)
  #:use-module (website types)
  #:use-module (website apps base contribute-page)
  #:use-module (website apps base download-page)
  #:use-module (website apps base index-page)
  #:use-module (website apps base learn-page)
  #:use-module (website apps base libraries-page)
  #:export (base-resources))


(define base-resources
  (list (make-web-resource "index.html" index-page)
	(make-web-resource "contribute/index.html" (contribute-page))
	(make-web-resource "download/index.html" (download-page))
	(make-web-resource "guile.html" index-page)
	(make-web-resource "learn/index.html" (learn-page))
	(make-web-resource "libraries/index.html" (libraries-page))))
