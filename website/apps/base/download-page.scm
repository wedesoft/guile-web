;;; Guile website --- GNU's extension language website
;;; Copyright © 2015 Luis Felipe López Acevedo <felipe.lopez@openmailbox.org>
;;;
;;; This file is part of Guile website.
;;;
;;; Guile website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Guile website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (website apps base download-page)
  #:use-module (website apps base components)
  #:use-module (website utils)
  #:export (download-page))


(define (download-page)
  "Return the download page in SXML."
  `(html
    (@ (lang "en"))
    ,(html-head "Download"
		#:css (static-url "base/css/download.css"))
    (body
     ,(site-menu #:active-link "Download")
     (main
      (section
       (@ (class "page-summary centered noise-bg"))
       (h1 ,(string-append "Download Guile " (latest-guile-version)))
       (p "Try installing Guile from your "
	  (a (@ (href "#packages")) "system packages") " first. It is likely "
	  "that Guile is already packaged for the operating system you are "
	  "using. This is the easiest and the recommended way of installing "
	  "Guile. You can also install from the "
	  (a (@ (href "#releases")) "released tarballs") ", or cloning "
	  "Guile's source code from the "
	  (a (@ (href "#repository")) "Git repository") "."))
      (div
       (@ (class "relative"))
       (nav
	(@ (class "toc"))
	(h2 "Contents")
	(ul
	 (li
	  (a (@ (href "#packages")) "Installing packaged Guile")
	  (ul
	   (li (a (@ (href "#guix")) "Guix"))
	   (li (a (@ (href "#debian")) "Debian"))
	   (li (a (@ (href "#parabola")) "Parabola"))))
	 (li (a (@ (href "#releases")) "Releases"))
	 (li (a (@ (href "#repository")) "Repository"))
	 (li (a (@ (href "#snapshots")) "Snapshots"))))
       (section
	(@ (class "sheet"))
	(h2 (@ (id "packages")) "Installing packaged Guile")
	(h3 (@ (id "guix")) "Guix")
	(P "If you use "
	   (a (@ (href "http://www.gnu.org/software/guix/")) "GNU Guix")
	   " on GuixSD or on top of a GNU/Linux distribution, run the "
	   "following command:")
	(pre
	 (@ (class "shell"))
	 "guix package --install guile")
	(h3 (@ (id "debian")) "Trisquel, gNewSense, Debian, etc.")
	(P "If you use " (a (@ (href "http://trisquel.info/")) "Trisquel")
           ", " (a (@ (href "http://gnewsense.org")) "gNewSense")", "
	   " or other Debian derivatives, run the following command:")
	(pre
	 (@ (class "shell"))
	 "apt-get install guile-2.2")
        (p "To get the older 2.0 series, try:")
	(pre
	 (@ (class "shell"))
	 "apt-get install guile-2.0")
        (h3 (@ (id "parabola")) "Parabola")
	(P "If you use " (a (@ (href "http://www.parabola.nu/")) "Parabola")
           ", run the following command:")
	(pre
	 (@ (class "shell"))
	 "pacman -S guile")
	(h2 (@ (id "releases")) "Releases")
	(P "The latest releases of Guile are available on the web. "
	   "The " (a (@ (href "http://www.gnu.org/software/software.html"))
		     "GNU project's software")
	   " page has full details about mirror sites, etc. ")
	(p "Stable and maintenance releases of Guile have even minor version "
	   "numbers, and can be downloaded from "
	   (a (@ (href "https://ftp.gnu.org/gnu/guile/"))
	      "https://ftp.gnu.org/gnu/guile/") ":")
	(ul
	 (li "The latest release of Guile's 2.2.x series is "
	     (a (@ (href ,(string-append
                           "https://ftp.gnu.org/gnu/guile/guile-"
                           (latest-guile-version)
                           ".tar.gz")))
		,(latest-guile-version)))
	 (li "The latest release of Guile's legacy 2.0.x series is "
	     (a (@ (href "https://ftp.gnu.org/gnu/guile/guile-2.0.14.tar.gz"))
		"2.0.14")))
	(p "Development releases of Guile are given odd minor version "
	   "numbers, and can be downloaded from "
	   (a (@ (href "http://alpha.gnu.org/gnu/guile/"))
	      "http://alpha.gnu.org/gnu/guile/") ".")
	(h2 (@ (id "repository")) "Repository")
	(P "Since March 27th, 2008, Guile's source code is stored in a "
	   (a (@ (href "http://git-scm.com/")) "Git") " repository at "
	   "Savannah. You can access it with the following command: ")
	(pre
	 (@ (class "shell"))
	 "git clone git://git.sv.gnu.org/guile.git")
	(p "If the port used by the " (code "git") " protocol is not "
	   "available, you can instead use the (slower) " (code "http")
	   " method:")
	(pre
	 (@ (class "shell"))
	 "git clone http://git.sv.gnu.org/r/guile.git")
	(p "Developers with a "
	   (a (@ (href "http://savannah.gnu.org/")) "Savannah") " account "
	   "can access the repository over SSH:")
	(pre
	 (@ (class "shell"))
	 "git clone ssh://git.sv.gnu.org/srv/git/guile.git")
	(p "The repository can also be "
	   (a (@ (href "http://git.sv.gnu.org/gitweb/?p=guile.git"))
	      "browsed on-line") ".")
	(h2 (@ (id "snapshots")) "Snapshots")
	(P " Snapshots are available from our continuous integration system: ")
	(ul
	 (li (a (@ (href "http://hydra.nixos.org/job/gnu/guile-2-2/tarball/latest")) "2.2 stable series"))
	 (li (a (@ (href "http://hydra.nixos.org/job/gnu/guile-2-0/tarball/latest")) "2.0 old stable series"))
	 (li (a (@ (href "http://hydra.nixos.org/job/gnu/guile-master/tarball/latest")) "development, " (code "master") " branch"))))))
     ,(site-footer))))
