;;; Guile website --- GNU's extension language website
;;; Copyright © 2015 Luis Felipe López Acevedo <felipe.lopez@openmailbox.org>
;;; Copyright © 2015, 2016 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2015 Mathieu Lirzin <mthl@openmailbox.org>
;;;
;;; This file is part of Guile website.
;;;
;;; Guile website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Guile website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (website apps base news)
  #:use-module (sxml simple)
  #:use-module (website utils)
  #:use-module (website apps base components)
  #:use-module (haunt post)
  #:use-module (haunt site)
  #:use-module (haunt builder blog)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-19)
  #:use-module (ice-9 match)
  #:export (post-url
            post->summary-sxml
            %news-haunt-theme))

(define (post-url post site)
  "Return the URL of POST, a Haunt blog post, for SITE."
  (site-url (string-append "news/" (site-post-slug site post) ".html")))

(define (post->summary-sxml post url)
  "Return the an SXML tree representing POST, a Haunt blog post, with a link
to URL."
  `(a (@ (href ,url)
         (class "news-item"))
      (h4 ,(post-ref post 'title))
      (p (@ (class "news-date"))
         ,(date->string (post-date post) "~B ~e, ~Y"))
      (p (@ (class "news-summary"))
         ,(summarize-string (sxml->string* (post-sxml post))
                            170)
         "…")))

(define (sxml->string* tree)
  "Flatten tree by dismissing tags and attributes, and return the resulting
string."
  (define (sxml->strings tree)
    (match tree
      (((? symbol?) ('@ _ ...) body ...)
       (append-map sxml->strings body))
      (((? symbol?) body ...)
       (append-map sxml->strings body))
      ((? string?)
       (list tree))))

  (string-concatenate (sxml->strings tree)))

(define (summarize-string str n)
  "Truncate STR at the first space encountered starting from the Nth
character."
  (if (<= (string-length str) n)
      str
      (let ((space (string-index str #\space n)))
        (string-take str (or space n)))))


;;;
;;; Theme.
;;;

(define* (post->sxml post #:key post-uri)
  "Return the SXML for POST."
  `(section (@ (class "sheet"))
            (h2 (@ (class "title"))
                ,(if post-uri
                     `(a (@ (href ,post-uri))
                         ,(post-ref post 'title))
                     (post-ref post 'title)))
            (div (@ (class "post-about"))
                 ,(post-ref post 'author)
                 " — " ,(date->string (post-date post) "~B ~e, ~Y"))
            (div (@ (class "post-body"))
                 ,(post-sxml post))))

(define (news-page-sxml site title posts prefix)
  "Return the SXML for the news page of SITE, containing POSTS."
  `((section (@ (class "page-summary centered noise-bg"))
             (h1 "Recent News "
                 (a (@ (href ,(site-url "news/feed.xml")))
                    (img (@ (alt "Atom feed")
                            (src ,(static-url "base/img/feed.png")))))))
    (div (@ (class "post-list"))
         ,@(map (lambda (post)
                  (post->sxml post #:post-uri (post-url post site)))
                posts))))

(define (base-layout body)
  `(html (@ (lang "en"))
         ,(html-head "News"
                     #:css (static-url "base/css/news.css"))

	 (body
          ,(site-menu #:active-link "News")

	  (main (article ,body))

	  ,(site-footer))))

(define %news-haunt-theme
  ;; Theme for the rendering of the news pages.
  (theme #:name "GNU Guile"
         #:layout (lambda (site title body)
                    (base-layout body))
         #:post-template post->sxml
         #:collection-template news-page-sxml))
