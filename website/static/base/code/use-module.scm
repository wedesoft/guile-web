;;; Import a time/date library and display
;;; the current date and time in the format
;;; DAY, MONTH YEAR HOUR:SECOND
(use-modules (srfi srfi-19))

(display (date->string (current-date)
		       "~A, ~B ~Y ~H:~S"))
(newline)

